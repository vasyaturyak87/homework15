console.log("FIRST SCRIPT");
const person = {
    name: "Alex",
    age: 35,
    gender: 'male'
};
console.log("PERSON");
console.log(person);

const newPerson = {
    name: person['name'],
    age: person.age,
    gender: person['gender']
};
console.log("NEW PERSON");
console.log(newPerson);

newPerson.age = 45;
console.log("NEW PERSON + change age");
console.log(newPerson);

console.log("______");
console.log("SECOND SCRIPT");
const car = {
    name: "Toyota",
    model: 'Corola',
    year: '2000'
};

const EV = {
    name: "Tesla",
    model: 'Model Y',
    year: '2023'
};

let resultOne = printCarInfo(car);
console.log(resultOne);

let resultTwo = printCarInfo(EV);
console.log(resultTwo);

function printCarInfo(x) {
    console.log('Make: ' + x.name + ', ' + 'Model: ' + x.model + ', ' + 'year: ' + x.year);
    x.year <= 2001 ? console.log('Old car') : console.log('New car');
};

console.log("______");
console.log("THIRD SCRIPT");
let str = 'Putin Huylo. Glory to Ukraine'
console.log(str);
let Word = countWords(str);
console.log(Word);

function countWords(x) {
    return x.trim().split(/\s+/).length;
}

console.log("______");
console.log("FORTH SCRIPT");
let $randomText = 'Web development is a cool stuff'
console.log($randomText);
let $reversText = reversStr($randomText);
console.log($reversText);
function reversStr(x) {
    return x.split("").reverse().join("");
}
console.log("______");
console.log("FIFTH SCRIPT");
let $randomString = 'We are the best student in the world'
console.log($randomString);

let $reversWord = reversWord($randomString);
console.log($reversWord);
function reversWord(x) {
    let reversed;
    let newArray=[];
    reversed = x.split(" ");
    for(var i = 0;i<reversed.length; i++)
    {
        newArray.push(reversed[i].split("").reverse().join(""));
    }
    return newArray.join(" ");
}
